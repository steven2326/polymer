 /**
 * Created by SCuellar and MLoaiza on 02/04/2016.
 */
 "use strict";

 console.log("Seguimiento de app");

 var verificacion = "Verificando";
 console.log(verificacion);

 var mensajes = [];
 var mensajesFiltrados = [];

 $.ajax("http://udem.herokuapp.com/parcial" )
     .done(function(data){
         mensajes = data.messages;
         filtrarDatos("");
     }).fail(function(err) {
     console.log(err);
 });

var filtrarDatos = function(filtro) {
    //alert(filtro);
    if (!filtro || filtro == "") {
        mensajesFiltrados = mensajes;
    }else{
        mensajesFiltrados = [];
        for (var i = 0; i < mensajes.length ; i++) {
            if (mensajes[i].message.toLowerCase().include(filtro.toLowerCase())) {
                mensajesFiltrados.push(mensajes[i]);
            }
        }
    }
    mostrarDatos(mensajesFiltrados);
}
 
 var mostrarDatos = function (messages){
     for(var i=0; i< messages.length; i++) {
         var datos = messages[i];
         var cant = messages.length;
         console.log(datos);
         console.log (cant);
         var foto = messages[i].picture;
         $('.messages')
             .append('' +
                 '<p-message picture="'+ ((!foto.trim()) ? "images/no_pic.jpg" : messages[i].picture) +'"   icon="'+  messages[i].icon +'"   message="'+ messages[i].message +'"></p-message>'
             );
     }
 };